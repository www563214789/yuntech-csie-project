#!/usr/bin/env python
# coding: utf-8

from keras.models import Sequential, Model
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Activation, Flatten, Dropout, Dense
from keras.utils import np_utils, plot_model

from keras.optimizers import Adam
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.resnet50 import preprocess_input, decode_predictions

import os
import cv2
import numpy as np

def classification(path, tlist, t_first, t_end):
    x_data = []
    y_label =[]
    fs = os.listdir(path)
    for f in fs:
        image_path = os.path.join(path, f)
        img = cv2.imread(image_path)
        x_data.append(img)
        
        t = f [t_first:t_end]
        
        for i in range(len(tlist)):
            if t == tlist[i]:
                y_label.append(i)
                break;
      
        x = np.array(x_data)/255
        y = np_utils.to_categorical(y_label)
        train = [x,y]
        
        print(x)
        print(y)
    return(train)

def data_generate(x,y):
    datagen = ImageDataGenerator(rotation_range=20,
                                width_shift_range=0.2,
                                height_shift_range=0.2,
                                horizontal_flip=True)
    return datagen.flow(x, y, batch_size=10)

def train_cnn():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding="same", input_shape=(48,48,3)))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, (3, 3), padding="same", input_shape=(48,48,3)))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))

    model.add(Flatten())
    
    model.add(Dense(512))
    model.add(Activation("relu"))
    model.add(BatchNormalization())
    model.add(Dropout(0.8))
    model.add(Dense(512))
    model.add(Activation("relu"))
    model.add(Dropout(0.2))
    model.add(Dense(7))
    model.add(Activation("softmax"))
    return model

def start_train(train):
    #選擇優化器
    model = train_cnn()
    opt = Adam(lr=0.01, decay=0.01)
    model.compile(loss ="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])

    #訓練開始
    history = model.fit(train[0],train[1],
                        batch_size=16,
                        verbose=1,
                        epochs=50,
                        validation_split=0.1)

    #評分
    score = model.evaluate(train[0], train[1], verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    return model

def img_predict(s_path, img_path, target_size, model):
    #預測
    if s_path == 1:
        img = image.load_img(img_path, target_size)
    else:
        img = img_path
    px = np.reshape(img,[1,48,48,3])
    preds = model.predict_classes(px)
    print (preds)
    return preds



#測試區
'''
tlist=['AN','DI','FE','HA','SA','SU','NE']
train = classification('dataset\jaffe_testf', tlist, 3, 5)

traing = data_generate(train[0],train[1])
print(traing[0])
print(len(train[0]))

model = start_train(train)

img_predict('hihi.jpg',(48, 48), model)
img_predict('haha.jpg',(48, 48), model)
img_predict('chichi.jpg',(48, 48), model)
img_predict('BJGO.jpg',(48, 48), model)
img_predict('K_face.jpg',(48, 48), model)
img_predict('lpika.jpg',(48, 48), model)
'''
#模型存取
#model.save('my_model.h5')