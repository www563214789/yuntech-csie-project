import os
import gc
from datetime import datetime
from subprocess import Popen
import multiprocessing as mp
import time
from flask import Blueprint, render_template, request, redirect, url_for, flash, current_app, jsonify, Response

from webui.extensions import facials, actions
from webui.forms import VideoForm
from webui.facial_camera import VideoCamera

webui_bp = Blueprint('webui', __name__)


def getFiles(path=None):
    files = os.listdir(path)
    files = [list(os.path.splitext(file))+[datetime.fromtimestamp(os.path.getctime(os.path.join(path, file))).strftime("%Y/%m/%d - %H:%M:%S")] for file in files]
    files.sort(key = lambda files: files[2])
    return files


def getAbsPath(path=None):
    basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    path = os.path.join(basedir, path)
    return path


def gen(camera):
    try:
        while True:
            # print('dalao')
            frame = camera.get_frame()
            yield (b'--frame\r\n'
                 b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
    except Exception as e:
        print(e)
        print('deleted')
        # del camera
        



@webui_bp.route('/', methods=('GET', 'POST'))
def index():
    return render_template('index.html')


@webui_bp.route('/camera')
def camera():
    return render_template('camera.html')


@webui_bp.route('/facial', methods=('GET', 'POST'))
def facial():
    # global camera 
    # camera.__del__()
    # print(id(camera))
    # del camera
    # gc.collect()
    form = VideoForm()

    if request.method == 'POST':
        video = request.files['video']
        facials.save(video)
        # set args
        source = facials.path(video.filename)
        dest = facials.path(video.filename).replace('.mp4', '_predicted.mp4')
        model = getAbsPath('emotion_classifier.h5')

        p = Popen('python3 webui/facial_video.py -v {} -r {} -m {}'.format(source, dest, model), shell=True)
        p.wait()

        url = facials.url(video.filename.replace('.mp4', '_predicted.mp4'))
        return url
    return render_template('facial.html', form=form, postURL=url_for('webui.facial'), filesURL=url_for('webui.facialfiles'))


@webui_bp.route('/action', methods=('GET', 'POST'))
def action():
    form = VideoForm()
    if request.method == 'POST':
        video = request.files['video']
        actions.save(video)

        # set args
        source = actions.path(video.filename)
        # dest = actions.path(video.filename).replace('.mp4', '_predicted.mp4')
        # model = getAbsPath('')
        path = getAbsPath('st-gcn/main2.py')
        config = getAbsPath('st-gcn/config/st_gcn/childrenAct/childrenAct_recognize_1frame_4type_j.yaml')
        p = Popen('python3  {} --video {} --config {} --frame_segment 1'.format(path, source, config), shell=True)
        p.wait()

        url = actions.url(video.filename.replace('.mp4', '_predicted.mp4'))
        return url
    return render_template('action.html', form=form, postURL=url_for('webui.action'), filesURL=url_for('webui.actionfiles'))

@webui_bp.route('/members', methods=('GET', 'POST'))
def members():
    return render_template('members.html')


@webui_bp.route('/facialfiles', methods=('GET', 'POST'))
def facialfiles():
    path = current_app.config['UPLOADED_FACIALS_DEST']
    files = getFiles(path)
    return render_template('files.html', fileset=facials, files=files)


@webui_bp.route('/actionfiles', methods=('GET', 'POST'))
def actionfiles():
    path = current_app.config['UPLOADED_ACTIONS_DEST']
    files = getFiles(path)
    return render_template('files.html', fileset=actions, files=files)

@webui_bp.route('/stream')
def stream():
    path = getAbsPath('emotion_classifier.h5')
    camera = VideoCamera(path)

    # camera
    # print(id(camera))

    return Response(gen(camera), mimetype='multipart/x-mixed-replace; boundary=frame')
