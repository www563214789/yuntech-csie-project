import os
import argparse
import imutils
import pickle
import cv2
import dlib
import numpy as np
from keras.preprocessing.image import img_to_array
from keras.models import load_model

emos = ['neutral', 'happy', 'sad', 'suprise', 'angry', 'fear', 'disgust']
rects =[]

def show(frame, timeF, count):
    global rects
    if count % timeF == 0:
        rects = []
        faces, scores, idx = detector.run(frame, 0)
        try:
            for i, d in enumerate(faces):
                x1 = d.left()
                y1 = d.top()
                x2 = d.right()
                y2 = d.bottom()
                
                # 圖片預處理
                face = frame[y1:y2, x1:x2]
                face = cv2.resize(face, (48, 48))
                face = face.astype('float') / 255.0
                face = img_to_array(face)
                face = np.expand_dims(face, axis=0)

                # 表情分類
                proba = model.predict(face)[0]
                idx = np.argmax(proba)
                label = emos[idx]

                # 框臉並標示分類結果
                rects.append([x1, y1, x2, y2, label])
                cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(frame, label, (x1, y1-10), cv2.FONT_HERSHEY_DUPLEX, 0.6, (255, 255, 255), 1, cv2.LINE_AA)
        except:
            pass
    else:
        try:
            for rect in rects:
                cv2.rectangle(frame, (rect[0], rect[1]), (rect[2], rect[3]), (255, 255, 255), 2, cv2.LINE_AA)
                cv2.putText(frame, rect[4], (rect[0], rect[1]-10), cv2.FONT_HERSHEY_DUPLEX, 0.6, (255, 255, 255), 1, cv2.LINE_AA)
        except:
            pass
    return frame

# 定義參數
ap = argparse.ArgumentParser()
ap.add_argument('-m', '--model', required=True, help='path to trained model model')
# ap.add_argument('-l', '--labelbin', required=True, help='path to label binarizer')
ap.add_argument('-i', '--image', required=False, help='path to input image')
ap.add_argument('-v', '--video', required=False, help='path to input video')
ap.add_argument('-r', '--result', required=False, help='result path to input file')
args = vars(ap.parse_args())

# 載入模型及標籤
model = load_model(args['model'])
# lb = pickle.loads(open(args['labelbin'], 'rb').read())
detector = dlib.get_frontal_face_detector()

if args['image']:
    frame = cv2.imread(args['image'])
    frame = show(frame)

    # 顯示結果並儲存
    cv2.imshow('Emotion Recognition', frame)
    cv2.imwrite('result.jpg', frame)
    
    # 釋放資源
    cv2.waitKey(0)
    cv2.destroyAllWindows()
else:
    if args['video']:
        cap = cv2.VideoCapture(args['video'])
    else:
        cap = cv2.VideoCapture(0)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 500)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 500)

    # 設定輸出影片格式
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fourcc = cv2.VideoWriter_fourcc(*'H264')
    out = cv2.VideoWriter(args['result'], fourcc, 20.0, (width, height))

    # 設定每幾幀處理一次
    timeF = 4
    count = 0

    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret is not True: break
        frame = show(frame, timeF, count)
        count += 1

        # 顯示結果
        cv2.imshow('Emotion Recognition', frame)
        out.write(frame)

        if cv2.waitKey(10) == 27:
            break

    # 釋放資源
    out.release()
    cap.release()
    # cv2.destroyAllWindows()
